const my_map = divShow => {

    var map = new Map();
    var obj = {};

    map.set({}, 12);
    map.set(obj, [3,4,5]);
    map.set(obj, [6,7,8]);

    divShow('size:' + map.size);
    divShow(map.get(obj));
    divShow( '---' );
    map.forEach((v,k)=>{
        divShow( JSON.stringify(k) + ':::' + JSON.stringify(v));
    });

    const set = new Set();
    const obj2 = {a:12};
    set.add([2,3]);
    set.add([5,3])
        .add(obj2);
    divShow( '---' );
    divShow( set.size );
    set.add(obj2); // 重複加入
    set.add([2,3]);
    divShow( set.size );
    divShow( '---' );
    set.forEach((v)=>{
        divShow( JSON.stringify(v) );
    });
};

export default  my_map;