import Person from './person';

class Employee extends Person {
    constructor(firstname, lastname){
        super(firstname, lastname);
    }
}

export default Employee;