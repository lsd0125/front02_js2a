class Person {

    constructor(firstname, lastname, age=20, gender='male'){
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
    }

    toString(){
        return this.firstname+ ' ' + this.lastname;
    }

    get fullname(){
        return this.firstname+ ' ' + this.lastname;
    }

    set fullname(name){
        const ar = name.split(' ');
        //console.log('name:' +name);
        this.firstname = ar[0];
        this.lastname = ar[1];
    }

    describe(){
        return `name: ${this.firstname} ${this.lastname}
age: ${this.age}
gender: ${this.gender}`;
    }
}

export default Person;