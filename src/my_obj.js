const my_obj = divShow=>{

    let [a, b, c] = [6,7,'hello'];
    const obj = {a, b, c};
    divShow(JSON.stringify(obj));

    // ES5 作法
    let tools = {
        func: function() {

        },
        func2: function(){

        }
    };
    //tools.func();
    // ES6 作法1
    let tools1 = {
        func(){

        }
    };
    // ES6 作法2
    let tools2 = {
        func: ()=>{

        },
        func2: (n=12)=>{

        }
    };


};

export default my_obj;