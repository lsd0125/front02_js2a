const my_destruct = divShow=>{
    const person = {
        name: 'bill',
        age: 28,
        gender: 'male'
    };

    let {age, name, gender} = person;
    let {a, b, c} = person;

    divShow(name);
    divShow(age);
    divShow(gender);
    divShow(a + ' ' + b + ' ' + c);

    const ar = [6, 9, 12];
    let [aa, bb, cc] = ar;
    divShow(aa + ' ' + bb + ' ' + cc);

    const ar2 = [
        ...ar,
        20
    ];
    divShow( ar2.toString());

};

export default my_destruct;